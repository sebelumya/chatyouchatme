import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  value?: Date;
  formRegister = new FormGroup({
    fullName: new FormControl('', Validators.required),
    // birthDay: new FormControl(new Date(), Validators.required),
    // isMale: new FormControl(false, Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
    confirmPassword: new FormControl('', [Validators.required]),
  });
  minDate: Date = new Date();
  maxDate: Date = new Date();
  invalidDates: Array<Date> = new Array<Date>();
  constructor(private userService: UserService, private router: Router) {}
  // primengCalendarInit() {
  //   let today = new Date();
  //   let month = today.getMonth();
  //   let year = today.getFullYear();
  //   let prevMonth = month === 0 ? 11 : month - 1;
  //   let prevYear = prevMonth === 11 ? year - 1 : year;
  //   let nextMonth = month === 11 ? 0 : month + 1;
  //   let nextYear = nextMonth === 0 ? year + 1 : year;
  //   this.minDate = new Date();
  //   this.minDate.setMonth(prevMonth);
  //   this.minDate.setFullYear(prevYear);
  //   this.maxDate = new Date();
  //   this.maxDate.setMonth(nextMonth);
  //   this.maxDate.setFullYear(nextYear);
  //   let invalidDate = new Date();
  //   invalidDate.setDate(today.getDate() - 1);
  //   this.invalidDates = [today, invalidDate];
  // }
  ngOnInit(): void {
    // this.primengCalendarInit();
  }
  onSubmit() {
    const userRegister = {
      email: this.formRegister.value.email ?? '',
      password: this.formRegister.value.password ?? '',
      name: this.formRegister.value.fullName ?? '',
    };
    if (userRegister.password.length < 6) {
      return alert('Password must be at least 6 characters');
    }
    if (userRegister.password !== this.formRegister.value.confirmPassword) {
      return alert('Passwords does not match');
    }

    this.userService.register(userRegister).subscribe((data: any) => {
      console.log(data);

      if (data?.message == 'Success register') {
        alert('you can login now');
        this.router.navigate(['login']);
      } else {
        alert(data?.message);
      }
    });
  }
}
