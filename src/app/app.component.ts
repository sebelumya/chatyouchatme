import { Component } from '@angular/core';
import { Router, Route } from '@angular/router';
import { PrimeNGConfig } from 'primeng/api';
import { interval, Observable, BehaviorSubject } from 'rxjs';
import { UserService } from './services/user.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  constructor(
    private primengConfig: PrimeNGConfig,
    private router: Router,
    private user: UserService
  ) {}
  timer = interval(1000);

  token = localStorage.getItem('access_token')!;
  logOut = false;
  firstLaunch = false;
  // isLogOut() {
  //   if (!this.token) {
  //     return this.logOut.next('hide');
  //   } else {
  //     return this.logOut.next('');
  //   }
  // }

  onLogOut() {
    localStorage.removeItem('access_token');
    localStorage.removeItem('receiver');
    this.router.navigate(['login']);
    this.user.isLogin.next(false);
    this.logOut = false;
  }

  ngOnInit() {
    this.primengConfig.ripple = true;
    this.firstLaunch = true;
    if (this.token && this.firstLaunch) {
      console.log(this.token, '<token');
      this.logOut = true;
      this.user.isLogin.subscribe((data) => {
        if (data) {
          this.logOut = data;
          console.log(data, 'isLogin data');
        }
      });
    } else {
      this.user.isLogin.subscribe((data) => {
        this.logOut = data;
        console.log(data, 'isLogin data');
      });
    }
  }

  title = 'DokuChat';
}
