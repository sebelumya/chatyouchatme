import { FormGroup, FormControl, Validators } from '@angular/forms';
import { interval, Subscription, window } from 'rxjs';
import { UserService } from '../services/user.service';
import { ChatService } from '../services/chat.service';
import {
  Component,
  OnInit,
  AfterViewChecked,
  ElementRef,
  ViewChild,
} from '@angular/core';
import { untilDestroyed } from '@ngneat/until-destroy';
import { OnDestroy } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit, AfterViewChecked, OnDestroy {
  constructor(private user: UserService, private chat: ChatService) {}
  chatForm = new FormGroup({
    chatText: new FormControl('', Validators.required),
  });
  @ViewChild('scrollMe') myScrollContainer!: ElementRef;
  timer = interval(1000);

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  scrollToBottom(): void {
    try {
      this.myScrollContainer.nativeElement.scrollTop =
        this.myScrollContainer.nativeElement.scrollHeight;
    } catch (err) {}
  }
  receiver: string = localStorage.getItem('receiver')!;
  sender: string = '';
  nameSender: string = '';
  users: any;
  chats: any;
  listenerChat = new Subscription();

  getChat(status: string) {
    this.listenerChat = this.timer.subscribe((data) => {
      if (data) {
        console.log(data, '<<getchat');
        this.chat.getChat(this.sender, this.receiver).subscribe((data) => {
          this.chats = data;
        });
      }
      console.log(this.receiver, 'receiver');

      this.scrollToBottom();
    });
  }

  sendId(id: string, name: string) {
    this.sender = id;
    this.nameSender = name;
    console.log(id);
    this.getChat('open');
  }

  classSender(id: string) {
    if (id === this.receiver) {
      return 'kiri';
    } else {
      return 'kanan';
    }
  }
  deleteChat(sender: string) {
    this.chat.deleteChat(sender).subscribe((data) => console.log(data));
    this.getChat('open');
  }
  onSend(id: string) {
    if (this.sender.length <= 0) {
      alert('Select a user to chat with');
    } else {
      this.chat
        .sendChat(id, this.receiver, this.chatForm.value.chatText!)
        .subscribe((data) => {
          console.log(data, '<<onSend');
          this.chatForm.reset();
          this.getChat('open');
        });
    }
  }

  getUser() {
    this.user.getUser()?.subscribe((data) => {
      console.log(data, '<<data');
      this.users = data;
      console.log(this.users, '<<users');
    });
  }

  ngOnInit(): void {
    this.getUser();
    this.user.receiver.subscribe((data) => {
      this.receiver = data;
    });
  }

  ngOnDestroy() {
    console.log('ini destroy');

    this.listenerChat.unsubscribe();
  }
}
