import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { UserLogin } from '../interface/interface';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  constructor(
    private router: Router,
    private userService: UserService,
    private user: UserService
  ) {}
  playAudio() {
    let audio = new Audio();
    audio.src = '../../assets/audio/audio.mp3';
    audio.load();
    audio.play();
  }
  formLogin = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required),
  });
  getProfile() {}
  ngOnInit(): void {}

  onSubmit(): void {
    const userLogin = {
      email: this.formLogin.value.email ?? '',
      password: this.formLogin.value.password ?? '',
    };

    this.userService.login(userLogin).subscribe((data: any) => {
      console.log(data);

      if (data?.message == 'success') {
        const token = localStorage.setItem('access_token', data.token);
        this.user.getProfile(data.token).subscribe((data: any) => {
          console.log(data.data, '<<data getprofile');

          localStorage.setItem('receiver', data.data._id);
          this.userService.receiver.next(data.data._id);
        });
        this.getProfile();
        console.log(typeof token);
        this.user.isLogin.next(true);
        this.router.navigate(['']);
      } else {
        alert(data?.message);
      }
    });
  }
}
