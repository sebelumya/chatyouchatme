import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserLogin, UserRegister } from '../interface/interface';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  isLogin: Subject<boolean> = new BehaviorSubject(false);
  receiver: Subject<string> = new BehaviorSubject('');

  getNode() {
    return this.isLogin.asObservable();
  }

  addNode(data: boolean) {
    this.isLogin.next(data);
  }
  constructor(private http: HttpClient) {}
  urlApi = 'https://doku-todo-app.herokuapp.com/user/';

  token = localStorage.getItem('access_token');

  getUser() {
    return this.http.get(`${this.urlApi}`);
  }
  getProfile(token: string) {
    return this.http.get(`${this.urlApi}profile`, {
      headers: { Authorization: `Bearer ${token}` },
    });
  }
  editUser(name: string, email: string) {
    return this.http.put(
      `${this.urlApi}profile`,
      { name, email },
      {
        headers: { Authorization: `Bearer ${this.token}` },
      }
    );
  }
  login({ email, password }: UserLogin) {
    return this.http.post(`${this.urlApi}login`, { email, password });
  }
  register({ email, password, name }: UserRegister) {
    return this.http.post(`${this.urlApi}register`, { email, password, name });
  }
}
