import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ChatService {
  constructor(private http: HttpClient) {}
  urlApi = 'https://doku-todo-app.herokuapp.com/';

  sendChat(sender: string, receiver: string, message: string) {
    console.log(sender, receiver, message, '<< data sendchat');

    return this.http.post(`${this.urlApi}chat`, { sender, receiver, message });
  }
  getChat(id: string, receiver: string) {
    return this.http.get(
      `${this.urlApi}chat?sender=${id}&receiver=${receiver}`
    );
  }
  deleteChat(id: string) {
    return this.http.delete(`${this.urlApi}chat/${id}`);
  }
}
