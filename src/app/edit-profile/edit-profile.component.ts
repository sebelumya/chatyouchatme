import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css'],
})
export class EditProfileComponent implements OnInit {
  formEdit = new FormGroup({
    // linkAvatar: new FormControl(''),
    fullName: new FormControl('', Validators.required),
    // birthDay: new FormControl(new Date(), Validators.required),
    // isMale: new FormControl(false, Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    // oldPassword: new FormControl('', [Validators.required]),
    // newPassword: new FormControl('', Validators.required),
    // confirmPassword: new FormControl('', Validators.required),
  });
  token = localStorage.getItem('access_token');
  minDate: Date = new Date();
  maxDate: Date = new Date();
  invalidDates: Array<Date> = new Array<Date>();

  constructor(private user: UserService, private router: Router) {}
  getProfile() {
    this.user.getProfile(this.token!).subscribe((data: any) => {
      console.log(data.data.email, 'getProfile email');
      this.formEdit.setValue({
        fullName: data.data.name,
        email: data.data.email,
      });
    });
  }
  primengCalendarInit() {
    let today = new Date();
    let month = today.getMonth();
    let year = today.getFullYear();
    let prevMonth = month === 0 ? 11 : month - 1;
    let prevYear = prevMonth === 11 ? year - 1 : year;
    let nextMonth = month === 11 ? 0 : month + 1;
    let nextYear = nextMonth === 0 ? year + 1 : year;
    this.minDate = new Date();
    this.minDate.setMonth(prevMonth);
    this.minDate.setFullYear(prevYear);
    this.maxDate = new Date();
    this.maxDate.setMonth(nextMonth);
    this.maxDate.setFullYear(nextYear);
    let invalidDate = new Date();
    invalidDate.setDate(today.getDate() - 1);
    this.invalidDates = [today, invalidDate];
  }

  ngOnInit(): void {
    this.primengCalendarInit();
    this.getProfile();
    console.log(
      this.formEdit.value.fullName,
      this.formEdit.value.email,
      '<<name, email 1 '
    );
  }


  onSubmit() {
    console.log(
      this.formEdit.value.fullName,
      this.formEdit.value.email,
      '<<name, email'
    );

    this.user
      .editUser(this.formEdit.value.fullName!, this.formEdit.value.email!)
      .subscribe((data) => {
        console.log(data, '<data onSubmit');
        this.getProfile();
        // this.router.navigate(['']);
      });
  }
}
